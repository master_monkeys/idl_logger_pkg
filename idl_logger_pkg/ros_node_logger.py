## General ##
import numpy as np
import atexit
from datetime import datetime
from pathlib import Path
import os

## Ros stuff ##
import rclpy
from rclpy.node import Node
from rclpy.time import Time

from geometry_msgs.msg import Vector3, Vector3Stamped, TwistWithCovarianceStamped, PoseWithCovarianceStamped

class LoggerNode(Node):
    
    def __init__(self):
        super().__init__('loggerNode')
     
        # Get current time & date, to be added to filenames
        loggerbegin = datetime.now()
        self.loggerbegin_string = loggerbegin.strftime("%y-%m-%d_%H-%M")

        # Create folder
        homeStr = str(Path.home())
        self.saveLocation = homeStr + "/rosLogs/" + self.loggerbegin_string

        # Make folder if it does not exist
        if not os.path.isdir(self.saveLocation):
            os.makedirs(self.saveLocation)


        # Define vectors to hold logged data
        ''' Pose_ned, [time, x, y, z, phi, theta, psi] '''
        self.__groundTruthPose_ned = np.zeros((1, 7), dtype = np.float32)
        self.__ekfPose_ned = np.zeros((1, 7), dtype = np.float32)
        self.__pfPose_ned = np.zeros((1, 5), dtype = np.float32)

        ''' EKF covariance matrix [6,6] as [1,36]'''
        self.__ekfPoseVar_ned = np.zeros((1,36), dtype = np.float32)

        ''' PF variance, only diagonal is populated, element [0, 7, 14, 35] '''
        self.__pfPoseVar_ned = np.zeros((1, 4), dtype=np.float32)

        ''' Vel_ned, [time, x, y, z, phi, theta, psi] '''
        self.__groundTruthVel_ned = np.zeros((1, 7), dtype = np.float32)
        self.__ekfVel_ned = np.zeros((1, 7), dtype = np.float32)
        self.__ekfVelVar_ned = np.zeros((1,36), dtype = np.float32)

        ''' ekfPosError, [time, x_e, y_e, z_e] '''
        self.__ekfPosError = np.zeros((1, 4), dtype = np.float32)
        ''' ekfPosError, [time, phi_e, theta_e, psi_e] '''
        self.__ekftHetaError = np.zeros((1, 4), dtype = np.float32)

        ''' ekf Bias '''
        self.__ekfBias = np.zeros((1,7), dtype = np.float32)
        self.__ekfBiasVar = np.zeros((1,6), dtype = np.float32)

        
        ## Define subscriptions ##
        # GazeboGT
        self.__groundTruthPoseNedSubscriber = self.create_subscription(PoseWithCovarianceStamped, '/gazeboGT/pose_ned', self.__gtPoseCallback, 10)
        self.__groundTruthVelNedSubscriber = self.create_subscription(TwistWithCovarianceStamped, '/gazeboGT/vel_ned', self.__gtVelCallback, 10)
        self.__groundTruthEKFPosErrorSubscriber = self.create_subscription(Vector3Stamped, '/gazeboGT/ekf_pos_error', self.__ekfPosErrorCallback, 10)
        self.__groundTruthEKFtHetaErrorSubscriber = self.create_subscription(Vector3Stamped, '/gazeboGT/ekf_tHeta_error', self.__ekftHetaErrorCallback, 10)

        # EKF
        self.__ekfPoseNedSubscriber = self.create_subscription(PoseWithCovarianceStamped, '/ekf/pose_ned', self.__ekfPoseCallback, 10)
        self.__ekfVelNedSubscriber = self.create_subscription(TwistWithCovarianceStamped, '/ekf/vel_level', self.__ekfVelCallback, 10)
        self.__ekfBiasSubscriber = self.create_subscription(TwistWithCovarianceStamped, '/ekf/sensor_bias', self.__ekfBiasCallback, 10)

        # PF
        self.__pfPoseNedSubscriber = self.create_subscription(PoseWithCovarianceStamped, '/pf/pose_ned', self.__pfPoseCallback, 10)

        # Get startTime of logger node
        timeNow = self.get_clock().now().seconds_nanoseconds()
        self.__startTime = timeNow[0] + timeNow[1]*10**(-9)

        # Print
        self.get_logger().info("Logger node online")

    def getTimeSinceLoggerStart(self):
        # Get new timestamp
        newTime = self.get_clock().now().seconds_nanoseconds()
        timeSecs = newTime[0] + newTime[1]*10**(-9) - self.__startTime

        return timeSecs

    def __gtPoseCallback(self, msg):
        # Parse data from pose message
        poseFromMessage = msg.pose.pose
        
        # Get Time since logger start
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        position = poseFromMessage.position
        orientation = poseFromMessage.orientation
        loggerValueVec = np.array([[timeSecs, position.x, position.y, position.z, orientation.x, orientation.y, orientation.z]])

        # Append to self.vector
        self.__groundTruthPose_ned = np.append(self.__groundTruthPose_ned, loggerValueVec, axis=0) 

    def __gtVelCallback(self, msg):
        # Parse data from velocity message
        velFromMsg = msg.twist.twist

        # Get new timestamp
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        linVel = velFromMsg.linear
        angVel = velFromMsg.angular
        loggerValueVec = np.array([[timeSecs, linVel.x, linVel.y, linVel.z, angVel.x, angVel.y, angVel.z]])

        # Append to self.vector
        self.__groundTruthVel_ned = np.append(self.__groundTruthVel_ned, loggerValueVec, axis=0)

    def __ekfPoseCallback(self, msg):
        # Parse data from pose message
        poseFromMessage = msg.pose.pose
        varFromMsg = msg.pose.covariance
        
        # Get Time since logger start
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        position = poseFromMessage.position
        orientation = poseFromMessage.orientation
        loggerValueVec = np.array([[timeSecs, position.x, position.y, position.z, orientation.x, orientation.y, orientation.z]])

        loggerCov = np.array(varFromMsg).reshape(1,36)

        # Append to self.vector
        self.__ekfPose_ned = np.append(self.__ekfPose_ned, loggerValueVec, axis = 0) 
        self.__ekfPoseVar_ned = np.append(self.__ekfPoseVar_ned, loggerCov, axis = 0)

    def __ekfVelCallback(self, msg):
        # Parse data from velocity message
        velFromMsg = msg.twist.twist
        varFromMsg = msg.twist.covariance

        # Get new timestamp
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        linVel = velFromMsg.linear
        angVel = velFromMsg.angular
        loggerValueVec = np.array([[timeSecs, linVel.x, linVel.y, linVel.z, angVel.x, angVel.y, angVel.z]])

        loggerCov = np.array(varFromMsg).reshape(1,36)

        # Append to self.vector
        self.__ekfVel_ned = np.append(self.__ekfVel_ned, loggerValueVec, axis = 0)
        self.__ekfVelVar_ned = np.append(self.__ekfVelVar_ned, loggerCov, axis = 0)

    def __ekfBiasCallback(self, msg):
        # Parse data from velocity message
        velFromMsg = msg.twist.twist
        varFromMsg = msg.twist.covariance

        # Get new timestamp
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        linVel = velFromMsg.linear
        angVel = velFromMsg.angular
        loggerValueVec = np.array([[timeSecs, linVel.x, linVel.y, linVel.z, angVel.x, angVel.y, angVel.z]])

        loggerCov = np.array([[varFromMsg[0], varFromMsg[7], varFromMsg[14], varFromMsg[21], varFromMsg[28], varFromMsg[35]]])

        # Append to self.vector
        self.__ekfBias = np.append(self.__ekfBias, loggerValueVec, axis = 0)
        self.__ekfBiasVar = np.append(self.__ekfBiasVar, loggerCov, axis = 0)

    def __ekfPosErrorCallback(self, msg):
        # Parse data from velocity message
        posFromMsg = msg.vector

        # Get new timestamp
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        loggerValueVec = np.array([[timeSecs, posFromMsg.x, posFromMsg.y, posFromMsg.z]])

        # Append to self.vector
        self.__ekfPosError = np.append(self.__ekfPosError, loggerValueVec, axis=0)

    def __ekftHetaErrorCallback(self, msg):
        # Parse data from velocity message
        posFromMsg = msg.vector

        # Get new timestamp
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        loggerValueVec = np.array([[timeSecs, posFromMsg.x, posFromMsg.y, posFromMsg.z]])

        # Append to self.vector
        self.__ekftHetaError = np.append(self.__ekftHetaError, loggerValueVec, axis=0)

    def __pfPoseCallback(self, msg):
        # Parse data from pose message
        poseFromMessage = msg.pose.pose
        varFromMsg = msg.pose.covariance
        
        # Get Time since logger start
        timeSecs = self.getTimeSinceLoggerStart()

        # Collect data into vector
        position = poseFromMessage.position
        orientation = poseFromMessage.orientation
        loggerValueVec = np.array([[timeSecs, position.x, position.y, position.z, orientation.z]])

        loggerCov = np.array([[varFromMsg[0],varFromMsg[7],varFromMsg[14],varFromMsg[35]]])

        # Append to self.vector
        self.__pfPose_ned = np.append(self.__pfPose_ned, loggerValueVec, axis = 0) 
        self.__pfPoseVar_ned = np.append(self.__pfPoseVar_ned, loggerCov, axis = 0)

    def __saveCSV(self, name, vals):	
        # Save array
        np.savetxt( self.saveLocation + '/' + name, 
                    vals,
                    delimiter =", ", 
                    fmt ='% s')

    def saveLogs(self):
        # Save the vectors as csv files in the destructor
        self.get_logger().info("Shutting down, saving data to CSV files")

        ## Ground truth pose ##
        GTPoseBanner = np.array([["Time", "Pos x", "Pos y", "Pos z", "phi", "theta", "psi"]])
        GTPoseCSVArray = np.append(GTPoseBanner, self.__groundTruthPose_ned[1:,:], axis = 0)

        # Save array
        self.__saveCSV("groundTruthPoseNED.csv", GTPoseCSVArray)


        ## Ground truth velocity ##
        GTVelBanner = np.array([["Time", "Vel x", "Vel y", "Vel z", "AngVel x", "AngVel y", "AngVel z"]])
        GTVelCSVArray = np.append(GTVelBanner, self.__groundTruthPose_ned[1:,:], axis = 0)

        # Save array
        self.__saveCSV("groundTruthVelNED.csv", GTVelCSVArray)


        ## EKF pose ##
        ekfPoseLogVec = np.append(self.__ekfPose_ned[1:,:], self.__ekfPoseVar_ned[1:,:], axis = 1)
        ekfPoseBanner = np.empty((1, ekfPoseLogVec.shape[1]), dtype=object)
        ekfPoseBanner[0,:] = ' '
        ekfPoseBanner[0, 0:8] = ["Time", "Pos x", "Pos y", "Pos z", "phi", "theta", "psi", "cov"]
        ekfPoseCSVArray = np.append(ekfPoseBanner, ekfPoseLogVec, axis = 0)

        # Save array
        self.__saveCSV("ekfPoseNED.csv", ekfPoseCSVArray)


        ## EKF velocity ##
        ekfVelLogVec = np.append(self.__ekfVel_ned[1:,:], self.__ekfVelVar_ned[1:,:], axis = 1)
        ekfVelBanner = np.empty((1, ekfVelLogVec.shape[1]), dtype=object)
        ekfVelBanner[0,:] = ' '
        ekfVelBanner[0, 0:8] = np.array([["Time", "Vel x", "Vel y", "Vel z", "AngVel x", "AngVel y", "AngVel z", "cov"]])
        ekfVelCSVArray = np.append(ekfVelBanner, ekfVelLogVec, axis = 0)

        # Save array
        self.__saveCSV("ekfVelNED.csv", ekfVelCSVArray)


        ## EKF Pose error ##
        ekfPosErrorBanner = np.array([["Time", "x_e", "y_e", "z_e"]])
        ekfPosErrorArray = np.append(ekfPosErrorBanner, self.__ekfPosError[1:,:], axis = 0)

        # Save array
        self.__saveCSV("ekfPosErrorNED.csv", ekfPosErrorArray)

        ## EKF tHeta error ##
        ekftHetaErrorBanner = np.array([["Time", "phi_e", "theta_e", "psi_e"]])
        ekftHetaErrorArray = np.append(ekftHetaErrorBanner, self.__ekftHetaError[1:,:], axis = 0)

        # Save array
        self.__saveCSV("ekftHetaErrorNED.csv", ekftHetaErrorArray)

        ## EKF pose ##
        pfPoseLogVec = np.append(self.__pfPose_ned[1:,:], self.__pfPoseVar_ned[1:,:], axis = 1)
        pfPoseBanner = np.empty((1, pfPoseLogVec.shape[1]), dtype=object)
        pfPoseBanner[0,:] = ' '
        pfPoseBanner[0, 0:6] = ["Time", "Pos x", "Pos y", "Pos z", "psi", "cov"]
        pfPoseCSVArray = np.append(pfPoseBanner, pfPoseLogVec, axis = 0)

        # Save array
        self.__saveCSV("pfPoseNED.csv", pfPoseCSVArray)

        ## EKF bias ##
        ekfBiasLogVec = np.append(self.__ekfBias[1:,:], self.__ekfBiasVar[1:,:], axis = 1)
        ekfBiasBanner = np.empty((1, ekfBiasLogVec.shape[1]), dtype=object)
        ekfBiasBanner[0,:] = ' '
        ekfBiasBanner[0, 0:8] = np.array([["Time", "b_x", "b_y", "b_z", "b_phi", "b_theta", "b_psi", "var"]])
        ekfBiasCSVArray = np.append(ekfBiasBanner, ekfBiasLogVec, axis = 0)

        # Save array
        self.__saveCSV("ekfBias.csv", ekfBiasCSVArray)

            
def main(args=None):
    # Init rclpy
    rclpy.init(args=args)

    # Creates Logger node
    logger = LoggerNode()

    # Register atexit func, saves csv logs at program exit (ctrl + C)
    atexit.register(logger.saveLogs)

    # Spins node to keep it alive
    rclpy.spin(logger)

    # Destroys node on shutdown
    logger.destroy_node()
    rclpy.shutdown()  



if __name__ == "__main__":
    main()